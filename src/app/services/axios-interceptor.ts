import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BASE_URL
})

function changeBaseUrl(newBaseUrl: string) {
  axiosInstance.defaults.baseURL = newBaseUrl
}

function setBearerToken(token: string) {
  axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

export { axiosInstance, changeBaseUrl, setBearerToken }
