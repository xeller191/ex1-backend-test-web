'use client'
import {
  FacebookAuthProvider,
  GoogleAuthProvider,
  onAuthStateChanged,
  signInWithPopup,
  signOut
} from 'firebase/auth'
import {
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useState
} from 'react'
import { auth } from '../firebase'

interface User {
  uid: string
  displayName: string | null
  email: string | null
}

interface AuthContextProps {
  user: User | null
  googleSignIn: () => void
  facebookSignIn: () => void
  logOut: () => void
}

interface AuthContextProviderProps {
  children: ReactNode
}

const AuthContext = createContext<AuthContextProps | undefined>(undefined)

export const AuthContextProvider: React.FC<AuthContextProviderProps> = ({
  children
}: AuthContextProviderProps) => {
  const [user, setUser] = useState<User | null>(null)

  const googleSignIn = () => {
    const provider = new GoogleAuthProvider()
    signInWithPopup(auth, provider)
  }

  const facebookSignIn = () => {
    const provider = new FacebookAuthProvider()
    signInWithPopup(auth, provider)
  }

  const logOut = () => {
    signOut(auth)
  }

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (currentUser) => {
      setUser(currentUser)
    })
    return () => unsubscribe()
  }, []) // ไม่ต้องนำ user เข้าไปใน dependencies array

  return (
    <AuthContext.Provider
      value={{ user, googleSignIn, logOut, facebookSignIn }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export const useUserAuth = (): AuthContextProps => {
  const context = useContext(AuthContext)
  if (!context) {
    throw new Error('useUserAuth must be used within an AuthContextProvider')
  }
  return context
}
