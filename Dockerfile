# Install dependencies only when needed
FROM node:20-alpine3.18 AS base
RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY package*.json ./
RUN npm ci

# Rebuild the source code only when needed
FROM node:20-alpine3.18  AS builder
WORKDIR /app
COPY . .
COPY --from=base /app/node_modules ./node_modules
RUN mv .env .env
ENV NODE_ENV production
ENV APP_ENV Production

# Updated npm version
RUN npm install -g npm@10.2.5

RUN npm run build

# Production image, copy all the files and run next
FROM node:20-alpine3.18 AS runner
WORKDIR /app
ENV NODE_ENV production
ENV APP_ENV Production

# You only need to copy next.config.js if you are NOT using the default configuration
# EX COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/public ./public
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/next-start.sh ./start.sh

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system nextjs 1001
RUN chown -R nextjs:nodejs /app/.next
RUN chmod +x /app/start.sh

RUN apk add bash gettext

USER nextjs

EXPOSE 4000

ENTRYPOINT ["/app/start.sh"]