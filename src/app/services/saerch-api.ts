import { SearchApiResponse } from '../types/saerch-api-response'
import { axiosInstance } from './axios-interceptor'

export async function searchApi(
  searchQuery: string
): Promise<SearchApiResponse[]> {
  try {
    const response = await axiosInstance.get(
      `/search-api?search=${searchQuery}`
    )
    return response.data.result
  } catch (error: any) {
    throw new Error(error)
  }
}
