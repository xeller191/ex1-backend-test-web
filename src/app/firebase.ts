// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'
const firebaseConfig = {
  apiKey: 'AIzaSyAcZ7-eAkYsb5DBSV32--CSbkgkLopFBLg',
  authDomain: 'test-nextjs-ep1.firebaseapp.com',
  projectId: 'test-nextjs-ep1',
  storageBucket: 'test-nextjs-ep1.appspot.com',
  messagingSenderId: '943144671523',
  appId: '1:943144671523:web:c89b527cc396140d4c5496'
}

const app = initializeApp(firebaseConfig)
export const auth = getAuth(app)
