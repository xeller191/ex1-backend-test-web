'use client'
import { useCallback, useEffect, useState } from 'react'
import { useUserAuth } from './context/AuthContext'
import { setBearerToken } from './services/axios-interceptor'
import { searchApi } from './services/saerch-api'
import { SearchApiResponse } from './types/saerch-api-response'
export default function Home() {
  const { user } = useUserAuth()
  const [searchData, setSearchData] = useState<SearchApiResponse[]>([])
  console.log({ user })
  console.log({ baseUrl: process.env.NEXT_PUBLIC_BASE_URL })

  const searchApiData = useCallback(async () => {
    const result = await searchApi('art')
    setSearchData(result)
    console.log({ result })
  }, [])

  useEffect(() => {
    // @ts-ignore
    if (user && user?.accessToken) {
      // @ts-ignore
      setBearerToken(user?.accessToken)
      searchApiData()
    }
  }, [user, searchApiData])

  return (
    <main className="p-4">
      <h1>Token:</h1>
      {/* @ts-ignore */}
      <h1>{JSON.stringify(user?.accessToken)}</h1>
      <h1>-------------------------------</h1>
      {searchData.map((item) => (
        <div>
          <h1>{item.name}</h1>
        </div>
      ))}
    </main>
  )
}
